import pandas as pd

from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import VotingClassifier
from sklearn.svm import LinearSVC
from sklearn.calibration import CalibratedClassifierCV
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy import sparse


from converter import convert


def get_predict(train_files, test_file):
    df = convert(train_files)
    # df.fillna(inplace=True)
    # df.to_csv('human_or_bot_train.csv', sep="\t", encoding='utf8')
    df_local = convert([test_file], is_test=True)
    # df_local.to_csv('human_or_bot_valid.csv', sep="\t", encoding='utf8')

    humans = df.loc[(df.alice_is_bot == 0)].response_alice.values.tolist() + \
             df.loc[(df.bob_is_bot == 0)].response_bob.values.tolist()
    humans = [human for human in humans if human]
    # humans = filter(lambda x: isinstance(x, str), humans)

    bots = df.loc[(df.alice_is_bot == 1)].response_alice.values.tolist() + \
           df.loc[(df.bob_is_bot == 1)].response_bob.values.tolist()
    bots = [human for human in bots if human]  #  is not None and isinstance(human, str)

    train = pd.DataFrame()
    train['text'] = pd.Series(humans + bots)
    train['label'] = pd.Series([0] * len(humans) + [1] * len(bots))
    train = train.sample(frac=1).reset_index(drop=True)

    clf1 = LogisticRegression(random_state=1)
    clf2 = CalibratedClassifierCV(LinearSVC())
    eclf = VotingClassifier(estimators=[('lr', clf1), ('svm', clf2)], voting='soft')
    all_train = train.text.values.tolist()
    all_y = train.label.values

    vectorizer1 = TfidfVectorizer(analyzer='char', ngram_range=(3, 5), max_features=50000)
    vectorizer2 = TfidfVectorizer(ngram_range=(1, 3), min_df=2, max_features=50000)
    tfidf_train1 = vectorizer1.fit_transform(all_train)
    tfidf_train2 = vectorizer2.fit_transform(all_train)
    tfidf_train = sparse.hstack([tfidf_train1, tfidf_train2])

    # print(tfidf_train.shape)
    # for clf, label in zip([clf1, clf2, eclf], ['Logistic Regression', 'SVM', 'Ensemble']):
    #     scores = cross_val_score(clf, tfidf_train.toarray(), all_y, cv=5, scoring='roc_auc')
    #     print("ROC-AUC: %0.2f (+/- %0.2f) [%s]" % (scores.mean(), scores.std(), label))

    '''humans_test = df_local.loc[df_local.alice_is_bot == 0].response_alice.values.tolist() + \
                  df_local.loc[(df_local.bob_is_bot == 0)].response_bob.values.tolist()
    humans_test = [human for human in humans_test if human]

    bots_test = df_local.loc[df_local.alice_is_bot == 1].response_alice.values.tolist() + \
                df_local.loc[df_local.bob_is_bot == 1].response_bob.values.tolist()
    bots_test = [human for human in bots_test if human]

    test = pd.DataFrame()
    test['text'] = pd.Series(humans_test + bots_test)
    test['label'] = pd.Series([0] * len(humans_test) + [1] * len(bots_test))
    test = test.sample(frac=1).reset_index(drop=True)

    all_test = test.text.values.tolist()
    # all_y_test = test.label.values

    test_vec = sparse.hstack([vectorizer1.transform(all_test), vectorizer2.transform(all_test)])'''

    eclf.fit(tfidf_train, all_y)

    # y_pred = eclf.predict_proba(test_vec)
    # from sklearn.metrics import roc_auc_score
    # roc_auc_score(all_y_test, y_pred[:, 1])

    result = []
    for idx, row in df_local.iterrows():
        alice_utt = row['response_alice']
        alice_utt_vec = sparse.hstack([vectorizer1.transform([alice_utt]), vectorizer2.transform([alice_utt])])
        alice_bot_prob = eclf.predict_proba(alice_utt_vec)[:, 1][0]

        bob_utt = row['response_bob']
        bob_utt_vec = sparse.hstack([vectorizer1.transform([bob_utt]), vectorizer2.transform([bob_utt])])
        bob_bot_prob = eclf.predict_proba(bob_utt_vec)[:, 1][0]

        dialogue_id = row['dialogId']

        result.append({'dialogId': dialogue_id, 'alice_bot_prob': alice_bot_prob, 'bob_bot_prob': bob_bot_prob})

    result = pd.DataFrame(result)

    result.to_csv('bot_prediction.csv', index=False)


if __name__ == '__main__':
    train_files = ['dialogues_24.json', 'dialogues_25.json', 'dialogues_26.json']
    test_file = 'dialogues_27.json'
    get_predict(train_files, test_file)
