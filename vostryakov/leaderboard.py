#!/usr/bin/env python3

from scipy.stats import spearmanr
import sys
import json
import csv


def score(file_labals, file_test):
    with open(file_test, 'r') as csvfile:
        lines = csvfile.readlines()

    users_bot_flags = {}
    for line in lines:
        items = json.loads(line)
        for d in items:
            if d['evaluation'][0]['userId'] == 'Alice':
                users_bot_flags[d['dialogId']] = ( (int(d['evaluation'][0]['quality']), int(d['evaluation'][1]['quality'])) )
            else:
                users_bot_flags[d['dialogId']] = ( (int(d['evaluation'][1]['quality']), int(d['evaluation'][0]['quality'])) )

    users_bot_predicted_probs = []
    users_bot_fact_labels = []
    with open(file_labals, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        is_head = True
        for row in spamreader:
            if is_head:
                is_head = False
                continue
            dialog = int(row[0])
            if dialog in users_bot_flags:
                users_bot_fact_labels.append(users_bot_flags[dialog][0])
                users_bot_predicted_probs.append(float(row[1]))

                users_bot_fact_labels.append(users_bot_flags[dialog][1])
                users_bot_predicted_probs.append(float(row[2]))
            else:
                print("dialog %s not in dataset" % dialog, file=sys.stderr)

    print(spearmanr(users_bot_fact_labels, users_bot_predicted_probs).correlation)


if __name__ == '__main__':
    score('submit_test_all.csv', 'data/dialogues_26.json')
