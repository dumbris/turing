import sys
import zlib

import distance
import edlib
from fuzzywuzzy import fuzz
import lightgbm as lgb
import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix, hstack
from scipy.stats import pearsonr, spearmanr
from sklearn.feature_extraction.text import TfidfVectorizer, ENGLISH_STOP_WORDS
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split, cross_val_score

from bot_classifier import get_predict
from converter import convert, combine_alice_and_bob, set_zero_where_bot
from leaderboard import score

DATA_FOLDER = 'data'


def save_sparse_csr(array, filename):
    np.savez(filename, data=array.data, indices=array.indices,
             indptr=array.indptr, shape=array.shape)


def word_match_alice(row):
    c_words = {word for word in str(row['context']).split() if word not in ENGLISH_STOP_WORDS}
    r_words = {word for word in str(row['response_alice']).split() if word not in ENGLISH_STOP_WORDS}
    if not c_words and not r_words:
        return 0
    return len(c_words & r_words) / float((len(c_words) + len(r_words)))


def word_match_bob(row):
    c_words = {word for word in str(row['context']).split() if word not in ENGLISH_STOP_WORDS}
    r_words = {word for word in str(row['response_bob']).split() if word not in ENGLISH_STOP_WORDS}
    if not c_words and not r_words:
        return 0
    return len(c_words & r_words) / float((len(c_words) + len(r_words)))


def word_match_alice_bob(row):
    c_words = {word for word in str(row['response_bob']).split() if word not in ENGLISH_STOP_WORDS}
    r_words = {word for word in str(row['response_alice']).split() if word not in ENGLISH_STOP_WORDS}
    if not c_words and not r_words:
        return 0
    return len(c_words & r_words) / float((len(c_words) + len(r_words)))


def lev(x):
    r = edlib.align(x['response_alice'], x['response_bob'])
    return r["editDistance"]


def lev_alice(x):
    r = edlib.align(x['context'], x['response_alice'])
    return r["editDistance"]


def sor(x):
    return 1 - distance.sorensen(x['response_alice'], x['response_bob'])


def jac(x):
    if x['response_bob'] and x['response_alice']:
        return 1 - distance.jaccard(x['response_alice'], x['response_bob'])
    else:
        return 0


def compressed_len(s):
    # return len(zlib.compress(s))  # python 2
    return len(zlib.compress(bytearray(s, 'utf-8')))  # python 3


# Normalized compression distance
def ncd(x):
    C_x = compressed_len(x['response_alice'])
    C_y = compressed_len(x['response_bob'])
    C_xy = compressed_len("".join([x['response_alice'], x['response_bob']]))
    return (C_xy - min(C_x, C_y)) / float(max(C_x, C_y))


def prepare_data(train_files, test_file, model_type='word'):
    print('Loading data')
    tm = convert(train_files)
    tem = convert([test_file], is_test=True)

    if model_type.startswith('char'):
        tfidf_context = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char')
        tfidf_alise_response = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char')
        tfidf_bob_response = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char')
    else:
        tfidf_context = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word',
                                        tokenizer=lambda x: x.split(' '), token_pattern='\\S+')
        tfidf_alise_response = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word',
                                               tokenizer=lambda x: x.split(' '), token_pattern='\\S+')
        tfidf_bob_response = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word',
                                             tokenizer=lambda x: x.split(' '), token_pattern='\\S+')
    tm.fillna('', inplace=True)
    tfidf_context.fit(tm['context'].values)
    tfidf_alise_response.fit(tm['response_alice'].values)
    tfidf_bob_response.fit(tm['response_bob'].values)

    # filter out only examples where alice or bob are a human and concat these data frames together
    if MODEL != 'all':
        tm = tm[tm[MODEL + '_is_bot'] == 0]
        pass
    else:
        tm_alice = tm[tm['alice_is_bot'] == 0]  # take in account only human evaluation
        tm_alice['all_eval'] = tm_alice['alice_eval'].values
        tm_bob = tm[tm['bob_is_bot'] == 0]  # take in account only human evaluation
        tm_bob['all_eval'] = tm_bob['bob_eval'].values
        tm = pd.concat([tm_alice, tm_bob])

    context_tfidf_train = tfidf_context.transform(tm['context'].values)
    alise_response_tfidf_train = tfidf_alise_response.transform(tm['response_alice'].values)
    bob_response_tfidf_train = tfidf_bob_response.transform(tm['response_bob'].values)

    print('Preparing final train dataset')
    data_tfidf_train = hstack([alise_response_tfidf_train, bob_response_tfidf_train,
                               np.expand_dims(tm['alice_uttrs_count'].values, 1),
                               np.expand_dims(tm['bob_uttrs_count'].values, 1),
                               np.expand_dims(tm['context'].str.len().values, 1),
                               np.expand_dims(tm['response_alice'].str.len().values, 1),
                               np.expand_dims(tm['response_bob'].str.len().values, 1),
                               np.expand_dims(tm['context'].str.split(' ').str.len().values, 1),
                               np.expand_dims(tm['response_alice'].str.split(' ').str.len().values, 1),
                               np.expand_dims(tm['response_bob'].str.split(' ').str.len().values, 1),
                               np.expand_dims(tm.apply(word_match_alice, axis=1, raw=True).values, 1),
                               np.expand_dims(tm.apply(word_match_bob, axis=1, raw=True).values, 1),
                               # np.expand_dims(tm.apply(jac, axis=1, raw=True).values, 1),
                               np.expand_dims(tm.apply(ncd, axis=1, raw=True).values, 1),
                               np.expand_dims(tm.apply(lambda x: fuzz.QRatio(str(x['response_alice']),
                                                                             str(x['response_bob'])), axis=1).values,
                                              1),
                               np.expand_dims(tm.apply(lambda x: fuzz.WRatio(str(x['response_alice']),
                                                                             str(x['response_bob'])), axis=1).values,
                                              1),
                               np.expand_dims(tm.apply(lambda x: fuzz.partial_ratio(str(x['response_alice']),
                                                                                    str(x['response_bob'])),
                                                       axis=1).values, 1),
                               np.expand_dims(tm.apply(lambda x: fuzz.partial_token_set_ratio(str(x['response_alice']),
                                                                                              str(x['response_bob'])),
                                                       axis=1).values, 1),
                               np.expand_dims(tm.apply(lambda x: fuzz.partial_token_sort_ratio(str(x['response_alice']),
                                                                                               str(x['response_bob'])),
                                                       axis=1).values, 1),
                               np.expand_dims(tm.apply(lambda x: fuzz.token_set_ratio(str(x['response_alice']),
                                                                                      str(x['response_bob'])),
                                                       axis=1).values, 1),
                               np.expand_dims(tm.apply(lambda x: fuzz.token_sort_ratio(str(x['response_alice']),
                                                                                       str(x['response_bob'])),
                                                       axis=1).values, 1),
                               ], format='csr').astype(np.float32)

    print('Preparing final test dataset')
    context_tfidf_test = tfidf_context.transform(tem['context'].values)
    alise_response_tfidf_test = tfidf_alise_response.transform(tem['response_alice'].values)
    bob_response_tfidf_test = tfidf_bob_response.transform(tem['response_bob'].values)
    data_tfidf_test = hstack([alise_response_tfidf_test, bob_response_tfidf_test,
                              np.expand_dims(tem['alice_uttrs_count'].values, 1),
                              np.expand_dims(tem['bob_uttrs_count'].values, 1),
                              np.expand_dims(tem['context'].str.len().values, 1),
                              np.expand_dims(tem['response_alice'].str.len().values, 1),
                              np.expand_dims(tem['response_bob'].str.len().values, 1),
                              np.expand_dims(tem['context'].str.split(' ').str.len().values, 1),
                              np.expand_dims(tem['response_alice'].str.split(' ').str.len().values, 1),
                              np.expand_dims(tem['response_bob'].str.split(' ').str.len().values, 1),
                              np.expand_dims(tem.apply(word_match_alice, axis=1, raw=True).values, 1),
                              np.expand_dims(tem.apply(word_match_bob, axis=1, raw=True).values, 1),
                              # np.expand_dims(tem.apply(jac, axis=1, raw=True).values, 1),
                              np.expand_dims(tem.apply(ncd, axis=1, raw=True).values, 1),
                              np.expand_dims(tem.apply(lambda x: fuzz.QRatio(str(x['response_alice']),
                                                                             str(x['response_bob'])), axis=1).values,
                                             1),
                              np.expand_dims(tem.apply(lambda x: fuzz.WRatio(str(x['response_alice']),
                                                                             str(x['response_bob'])), axis=1).values,
                                             1),
                              np.expand_dims(tem.apply(lambda x: fuzz.partial_ratio(str(x['response_alice']),
                                                                                    str(x['response_bob'])),
                                                       axis=1).values, 1),
                              np.expand_dims(tem.apply(lambda x: fuzz.partial_token_set_ratio(str(x['response_alice']),
                                                                                              str(x['response_bob'])),
                                                       axis=1).values, 1),
                              np.expand_dims(tem.apply(lambda x: fuzz.partial_token_sort_ratio(str(x['response_alice']),
                                                                                               str(x['response_bob'])),
                                                       axis=1).values, 1),
                              np.expand_dims(tem.apply(lambda x: fuzz.token_set_ratio(str(x['response_alice']),
                                                                                      str(x['response_bob'])),
                                                       axis=1).values, 1),
                              np.expand_dims(tem.apply(lambda x: fuzz.token_sort_ratio(str(x['response_alice']),
                                                                                       str(x['response_bob'])),
                                                       axis=1).values, 1),
                              ], format='csr').astype(np.float32)

    # Encode labels
    target_train = tm[MODEL + '_eval'].values
    # np.savez('target_train', data=target_valid)
    return data_tfidf_train, target_train, data_tfidf_test


def prepare_data_is_bot(train_files, test_file, model_type='word'):
    print('Loading data')
    tm = convert(train_files)
    tm.fillna('', inplace=True)
    new_tm = pd.DataFrame()
    new_tm['is_bot'] = pd.concat([tm['alice_is_bot'], tm['bob_is_bot']])
    new_tm['context'] = pd.concat([tm['context'], tm['context']])
    new_tm['response'] = pd.concat([tm['response_alice'], tm['response_bob']])
    tm = new_tm
    tfidf_context = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char', max_features=10000)
    tfidf_context_word = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word', max_features=10000)
    tfidf_response = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char', max_features=10000)
    tfidf_response_word = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word', max_features=10000)
    tfidf_context.fit(tm['context'].values)
    tfidf_context_word.fit(tm['context'].values)
    tfidf_response.fit(tm['response'].values)
    tfidf_response_word.fit(tm['response'].values)

    tem = convert([test_file], is_test=True)
    new_tem = pd.DataFrame()
    new_tem['context'] = pd.concat([tem['context'], tem['context']])
    new_tem['response'] = pd.concat([tem['response_alice'], tem['response_bob']])
    tem = new_tem

    '''df = pd.read_csv(DATA_FOLDER + 'human_or_bot_train.csv', sep='\t', encoding='utf-8', index_col=0)

    humans = df.loc[(df.alice_is_bot == 0)].response_alice.values.tolist() + \
             df.loc[(df.bob_is_bot == 0)].response_bob.values.tolist()
    humans = filter(None, humans)
    humans = filter(lambda x: isinstance(x, unicode), humans)

    #
    bots = df.loc[(df.alice_is_bot == 1)].response_alice.values.tolist() + \
           df.loc[(df.bob_is_bot == 1)].response_bob.values.tolist()
    bots = filter(None, bots)
    bots = filter(lambda x: isinstance(x, unicode), bots)

    #
    train = pd.DataFrame()
    train['text'] = pd.Series(humans + bots)
    train['label'] = pd.Series([0] * len(humans) + [1] * len(bots))
    train = train.sample(frac=1).reset_index(drop=True)
    train.head(10)'''

    '''if model_type.startswith('char'):
        tfidf_context = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char')
        tfidf_alise_response = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char')
        tfidf_bob_response = TfidfVectorizer(ngram_range=(3, 5), min_df=2, analyzer='char')
    else:
        tfidf_context = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word',
                                        tokenizer=lambda x: x.split(' '), token_pattern='\\S+')
        tfidf_alise_response = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word',
                                               tokenizer=lambda x: x.split(' '), token_pattern='\\S+')
        tfidf_bob_response = TfidfVectorizer(ngram_range=(1, 5), min_df=2, analyzer='word',
                                             tokenizer=lambda x: x.split(' '), token_pattern='\\S+')'''

    context_tfidf_train = tfidf_context.transform(tm['context'].values)
    context_tfidf_train_word = tfidf_context_word.transform(tm['context'].values)
    response_tfidf_train = tfidf_response.transform(tm['response'].values)
    response_tfidf_train_word = tfidf_response_word.transform(tm['response'].values)

    print('Preparing final train dataset')
    data_tfidf_train = hstack([# context_tfidf_train, context_tfidf_train_word,
                               response_tfidf_train, response_tfidf_train_word,
                               # np.expand_dims(tm['context'].str.len().values, 1),
                               # np.expand_dims(tm['context'].str.split(' ').str.len().values, 1),
                               np.expand_dims(tm['response'].str.len().values, 1),
                               np.expand_dims(tm['response'].str.split(' ').str.len().values, 1),
                               np.expand_dims(tm['response'].str.count('<eou>').values, 1),
                               # np.expand_dims(tm.apply(word_match_alice, axis=1, raw=True).values, 1),
                               # np.expand_dims(tm.apply(word_match_bob, axis=1, raw=True).values, 1),
                               # np.expand_dims(tm.apply(ncd, axis=1, raw=True).values, 1),
                               ], format='csr').astype(np.float32)

    print('Preparing final test dataset')
    context_tfidf_test = tfidf_context.transform(tem['context'].values)
    context_tfidf_test_word = tfidf_context_word.transform(tem['context'].values)
    response_tfidf_test = tfidf_response.transform(tem['response'].values)
    response_tfidf_test_word = tfidf_response_word.transform(tem['response'].values)
    data_tfidf_test = hstack([# context_tfidf_test, context_tfidf_test_word,
                              response_tfidf_test, response_tfidf_test_word,
                              # np.expand_dims(tem['context'].str.len().values, 1),
                              # np.expand_dims(tem['context'].str.split(' ').str.len().values, 1),
                              np.expand_dims(tem['response'].str.len().values, 1),
                              np.expand_dims(tem['response'].str.split(' ').str.len().values, 1),
                              np.expand_dims(tem['response'].str.count('<eou>').values, 1),
                              # np.expand_dims(tem.apply(word_match_alice, axis=1, raw=True).values, 1),
                              # np.expand_dims(tem.apply(word_match_bob, axis=1, raw=True).values, 1),
                              # np.expand_dims(tem.apply(ncd, axis=1, raw=True).values, 1),
                              ], format='csr').astype(np.float32)

    # Encode labels
    target_train = tm['is_bot'].values
    # np.savez('target_train', data=target_valid)
    return data_tfidf_train, target_train, data_tfidf_test


def train(x, y, x_test):
    print('Load train/validation data')
    test_preds = []

    for i in range(10):
        x_train, x_valid, y_train, y_valid = train_test_split(x, y, test_size=0.2, random_state=i * 1000)

        params = {'num_leaves': 128, 'num_trees': 1000, 'objective': 'regression_l2', 'data_random_seed': i * 1000,
                  'metric': ['mse'], 'boosting': 'gbdt', 'learning_rate': 0.1, 'verbosity': 0}

        train_data = lgb.Dataset(x_train, label=y_train, max_bin=255)
        valid_data = lgb.Dataset(x_valid, label=y_valid, max_bin=255, reference=train_data)

        num_round = 42
        bst = lgb.train(params, train_data, num_round, valid_sets=[valid_data], early_stopping_rounds=10)
        test_preds.append(bst.predict(x_test))

    print('Save a mean test predict')
    y_test_pred = np.mean(test_preds, axis=0)
    tem = convert([test_file], is_test=True)

    with open('submit_test_%s.csv' % MODEL, 'w') as f:
        f.write('dialogId,%s\n' % MODEL.title())
        for idx, y_row in enumerate(y_test_pred):
            f.write('%s,%s\n' % (tem['dialogId'][idx], y_row))

    def spearman_score(estimator, x_test, y_test):
        return spearmanr(y_test, estimator.predict(x_test)).correlation

    '''print()
    print('Cross-validation by lightgbm')
    num_round = 100
    train_data = lgb.Dataset(x, label=y, max_bin=255)
    print(lgb.cv(params, train_data, num_round, nfold=5, early_stopping_rounds=10, verbose_eval=False))'''

    '''print('Cross-validation by sklearn')
    r = lgb.LGBMRegressor(num_leaves=128, n_estimators=21, max_bin=255, objective='regression_l2',
                          boosting_type='gbdt', learning_rate=0.1, silent=True,
                          early_stopping_rounds=10)

    results = cross_val_score(r, x, y, cv=5, scoring=spearman_score,
                              fit_params={'early_stopping_rounds': 10, 'eval_metric': 'l2',
                                          'eval_set': [[x_valid, y_valid]]})
    print(results, np.mean(results))'''


def train_is_bot(x, y, x_test):
    print('Load train/validation data')

    test_preds = []
    for i in range(5):
        x_train, x_valid, y_train, y_valid = train_test_split(x, y, test_size=0.2, random_state=i * 1000)

        train_data = lgb.Dataset(x_train, label=y_train, max_bin=255)
        valid_data = lgb.Dataset(x_valid, label=y_valid, max_bin=255, reference=train_data)

        num_round = 50
        params = {'num_leaves': 128, 'num_trees': 1000, 'objective': 'binary', 'data_random_seed': i * 1000,
                  'metric': ['auc'], 'boosting': 'gbdt', 'learning_rate': 0.05, 'verbosity': 0}
        bst = lgb.train(params, train_data, num_round, valid_sets=[valid_data], early_stopping_rounds=5,
                        verbose_eval=1)

        test_preds.append(bst.predict(x_test))

    print('Save a mean test predict')
    y_test_pred = np.mean(test_preds, axis=0)
    alice_is_bot, bob_is_bot = np.split(y_test_pred, 2)
    tem = convert([test_file], is_test=True)
    with open('submit_test_is_bot.csv', 'w') as f:
        f.write('dialogId,Alice,Bob\n')
        idx = 0
        for alice_y, bob_y in zip(alice_is_bot, bob_is_bot):
            f.write('%s,%s,%s\n' % (tem['dialogId'][idx], alice_y, bob_y))
            idx += 1

    '''print('Cross-validation by lightgbm')
    num_round = 100
    train_data = lgb.Dataset(x, label=y, max_bin=255)
    print(lgb.cv(params, train_data, num_round, nfold=5, early_stopping_rounds=5, verbose_eval=False))'''

if __name__ == '__main__':
    print('A test file should be in data folder!')
    print('Command to start (without data folder!): python3 human_or_bot_classifier.py <test_file.json>')

    train_files = ['dialogues_24.json', 'dialogues_25.json', 'dialogues_26.json', 'dialogues_27.json']  #
    if len(sys.argv) == 2:
        test_file = sys.argv[1]
    else:
        test_file = 'dialogues_27.json'

    # TODO execute a classifier of a test file and get predictions for Alice and Bob
    print('\nTraining a classifier ...')
    get_predict(train_files, test_file)

    ''''print('Training a classifier...')
    represantation_type = 'word'
    MODEL = 'alice'
    x, y, x_test = prepare_data_is_bot(train_files, test_file, model_type=represantation_type)
    train_is_bot(x, y, x_test)

    MODEL = 'bob'
    x, y, x_test = prepare_data_is_bot(train_files, test_file, model_type=represantation_type)
    train_is_bot(x, y, x_test)

    combine_alice_and_bob('submit_alice_is_bot.csv', 'submit_bob_is_bot.csv', 'submit_test_is_bot.csv', opposite=True)'''

    print('\nTraining a regressors ...')
    represantation_type = 'char'
    MODEL = 'alice'
    x, y, x_test = prepare_data(train_files, test_file, model_type=represantation_type)
    train(x, y, x_test)
    MODEL = 'bob'
    x, y, x_test = prepare_data(train_files, test_file, model_type=represantation_type)
    train(x, y, x_test)

    combine_alice_and_bob('submit_test_alice.csv', 'submit_test_bob.csv', opposite=False)
    # score('submit_test_all.csv', DATA_FOLDER + '/' + test_file)

    print('Final predict with evaluation = 0 if a bot')
    set_zero_where_bot(test_file)
    # score('submit_test_reset.csv', DATA_FOLDER + '/' + test_file)

    print('Predictions were saved in submit_test_reset.csv')

    # mix(['results_test_words_split_with@@_08774.csv', 'subm_snli_0779_0438.csv', 'results_test_words_split_stemm.csv'],
    #    [0.50, 0.25, 0.25], 'submit_gbm_mix_08774_and_08717.csv')
