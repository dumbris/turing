from collections import defaultdict
import json
import os
import pandas as pd
import nltk
from random import randint

from leaderboard import score

# Initialize global variables
DATASETS_DIR = 'data'
TRAIN_FILES = 'dialogues_27.json'
END_OF_STRING = ' '
TOKEN_DELIMETR = ' '
ALICE_ID = 'Alice'
BOB_ID = 'Bob'

import pprint
pp = pprint.PrettyPrinter(indent=4)

tknzr = nltk.word_tokenize


def convert(files, is_test=False):
    dialogId = []
    context = []
    thread = []
    thread_alice = []
    thread_bob = []
    alice_is_bot = []
    bob_is_bot = []
    alice_eval = []
    bob_eval = []
    alice_uttrs_count = []
    bob_uttrs_count = []
    for file_name in files:
        with open(os.path.join(DATASETS_DIR, file_name), encoding='utf-8') as fh:
            data = json.load(fh)
            for row in data:
                context.append(TOKEN_DELIMETR.join(tknzr(row['context'])))
                dialogId.append(row['dialogId'])
                utts = []
                utts_alice = []
                utts_bob = []
                utts_alice_count = 0
                utts_bob_count = 0

                if not is_test:
                    users = {row['users'][0]['id']: row['users'][0]['userType'],
                             row['users'][1]['id']: row['users'][1]['userType']}
                    alice_is_bot.append(1 if users[ALICE_ID] == 'Bot' else 0)
                    bob_is_bot.append(1 if users[BOB_ID] == 'Bot' else 0)

                    user_evals = {row['evaluation'][0]['userId']: row['evaluation'][0]['quality'],
                                  row['evaluation'][1]['userId']: row['evaluation'][1]['quality']}
                    alice_eval.append(user_evals[ALICE_ID])
                    bob_eval.append(user_evals[BOB_ID])

                for utt in row["thread"]:
                    tokenized_text = TOKEN_DELIMETR.join(tknzr(utt["text"]))
                    utts.append('<%s> %s' % (utt["userId"], tokenized_text))
                    if utt["userId"] == ALICE_ID:
                        utts_alice.append('%s <eou>' % tokenized_text)
                        utts_alice_count += 1
                    if utt["userId"] == BOB_ID:
                        utts_bob.append('%s <eou>' % tokenized_text)
                        utts_bob_count += 1

                thread.append(' '.join(utts))
                thread_alice.append(END_OF_STRING.join(utts_alice))
                thread_bob.append(END_OF_STRING.join(utts_bob))
                alice_uttrs_count.append(utts_alice_count)
                bob_uttrs_count.append(utts_bob_count)

    if is_test:
        df = pd.DataFrame.from_dict({'dialogId': dialogId,
                                     'context': context,
                                     'response': thread,
                                     'response_alice': thread_alice,
                                     'response_bob': thread_bob,
                                     'alice_uttrs_count': alice_uttrs_count,
                                     'bob_uttrs_count': bob_uttrs_count
                                     }
                                    )
    else:
        df = pd.DataFrame.from_dict({'dialogId': dialogId,
                                     'context': context,
                                     'response': thread,
                                     'response_alice': thread_alice,
                                     'response_bob': thread_bob,
                                     'alice_is_bot': alice_is_bot,
                                     'bob_is_bot': bob_is_bot,
                                     'alice_eval': alice_eval,
                                     'bob_eval': bob_eval,
                                     'alice_uttrs_count': alice_uttrs_count,
                                     'bob_uttrs_count': bob_uttrs_count
                                     }
                                    )  # .set_index('dialogId')
    # df.to_csv(os.path.join(DATASETS_DIR, 'human_or_bot_train.csv'), sep="\t", encoding='utf8')
    return df


def analyse():
    dialogId = []
    context = []
    with open(os.path.join(DATASETS_DIR, TRAIN_FILES), encoding='utf-8') as fh:
        data = json.load(fh)
        utts_human = defaultdict(int)
        utts_bot = defaultdict(int)
        utts_human_thread = defaultdict(int)
        utts_bot_thread = defaultdict(int)
        utts_human_last = defaultdict(int)
        utts_bot_last = defaultdict(int)

        for row in data:
            context.append(TOKEN_DELIMETR.join(tknzr(row['context'])))
            dialogId.append(row['dialogId'])
            utts = []

            users = {row['users'][0]['id']: row['users'][0]['userType'],
                     row['users'][1]['id']: row['users'][1]['userType']}

            thread_human = []
            thread_bot = []

            for utt in row["thread"]:
                tokenized_text = TOKEN_DELIMETR.join(tknzr(utt["text"]))
                utts.append('%s: %s' % (utt["userId"], tokenized_text))
                if users[utt["userId"]] == 'Human':
                    utts_human[utt["text"]] += 1
                    thread_human.append(tokenized_text)
                if users[utt["userId"]] == 'Bot':
                    utts_bot[utt["text"]] += 1
                    thread_bot.append(tokenized_text)

            if users[utt["userId"]] == 'Human':
                utts_human_last[tokenized_text] += 1
            if users[utt["userId"]] == 'Bot':
                utts_bot_last[tokenized_text] += 1

            utts_human_thread[END_OF_STRING.join(thread_human)] += 1
            if thread_bot:
                utts_bot_thread[END_OF_STRING.join(thread_bot)] += 1

    '''print('Human utterances:')
    for idx, uttr in enumerate(sorted(utts_human, key=utts_human.get, reverse=True)):
        print(uttr, utts_human[uttr])
        if idx >= 100:
            break

    print()
    print('Bot utterances:')
    for idx, uttr in enumerate(sorted(utts_bot, key=utts_bot.get, reverse=True)):
        print(uttr, utts_bot[uttr])
        if idx >= 100:
            break'''

    for idx, uttr in enumerate(sorted(utts_human_last, key=utts_human_last.get, reverse=True)):
        print(uttr, utts_human_last[uttr])
        if idx >= 100:
            break

    print()
    print('Bot utterances:')
    for idx, uttr in enumerate(sorted(utts_bot_last, key=utts_bot_last.get, reverse=True)):
        print(uttr, utts_bot_last[uttr])
        if idx >= 100:
            break


def set_zero_where_bot(test_file):
        dialogId = []
        alice_is_bot = []
        bob_is_bot = []
        alice_eval = []
        bob_eval = []
        alice_thread = []
        bob_thread = []

        with open('bot_prediction.csv', encoding='utf-8') as csvfile:
            is_head = True
            idx = 0
            for line in csvfile:
                if is_head:
                    is_head = False
                    continue
                items = line.rstrip().split(',')
                dialogId.append(items[2])
                alice_is_bot.append(float(items[0]))
                bob_is_bot.append(float(items[1]))

        '''with open('submit_test_is_bot.csv', encoding='utf-8') as csvfile:
            is_head = True
            idx = 0
            for line in csvfile:
                if is_head:
                    is_head = False
                    continue
                items = line.rstrip().split(',')
                dialogId.append(items[0])
                alice_is_bot.append(float(items[1]))
                bob_is_bot.append(float(items[2]))'''

        '''with open(os.path.join(DATASETS_DIR, test_file), encoding='utf-8') as fh:
            data = json.load(fh)
            for row in data:
                dialogId.append(row['dialogId'])

                users = {row['users'][0]['id']: row['users'][0]['userType'],
                         row['users'][1]['id']: row['users'][1]['userType']}
                alice_is_bot.append(1 if users[ALICE_ID] == 'Bot' else 0)
                bob_is_bot.append(1 if users[BOB_ID] == 'Bot' else 0)

                user_evals = {row['evaluation'][0]['userId']: row['evaluation'][0]['quality'],
                              row['evaluation'][1]['userId']: row['evaluation'][1]['quality']}
                alice_eval.append(user_evals[ALICE_ID])
                bob_eval.append(user_evals[BOB_ID])

                utts_alice = []
                utts_bob = []
                for utt in row["thread"]:
                    tokenized_text = TOKEN_DELIMETR.join(tknzr(utt["text"]))
                    if utt["userId"] == ALICE_ID:
                        utts_alice.append('%s <eou>' % tokenized_text)
                    if utt["userId"] == BOB_ID:
                        utts_bob.append('%s <eou>' % tokenized_text)

                alice_thread.append(1 if utts_alice else 0)
                bob_thread.append(1 if utts_bob else 0)'''

        with open('submit_test_reset.csv', 'w') as out:
            with open('submit_test_all.csv', 'r') as csvfile:
                is_head = True
                idx = 0
                for line in csvfile:
                    if is_head:
                        is_head = False
                        out.write(line)
                        continue
                    items = line.rstrip().split(',')
                    if alice_is_bot[idx] > 0.5:  #  and randint(0, 100) < 50
                        items[2] = '0'
                    if bob_is_bot[idx] > 0.5:  #  and randint(0, 100) < 50
                        items[1] = '0'

                    out.write(','.join(items)+'\n')
                    idx += 1


def combine_alice_and_bob(alice_file, bob_file, output_file='submit_test_all.csv', opposite=True):
    alice = pd.read_csv(alice_file)
    bob = pd.read_csv(bob_file)
    alice_bob = pd.DataFrame()
    alice_bob['dialogId'] = alice['dialogId']
    if opposite:
        alice_bob['Bob'] = bob['Bob']
        alice_bob['Alice'] = alice['Alice']
    else:
        alice_bob['Alice'] = alice['Alice']
        alice_bob['Bob'] = bob['Bob']
    alice_bob.set_index('dialogId')
    alice_bob.to_csv(output_file, index=False)


if __name__ == '__main__':
    # analyse()
    # convert(TRAIN_FILES)
    # combine_alice_and_bob('submit_test_alice.csv', 'submit_test_bob.csv')
    test_file = 'dialogues_27.json'
    set_zero_where_bot(test_file)
    score('submit_test_all.csv', 'data/' + test_file)
    score('submit_test_reset.csv', 'data/' + test_file)
