# -*- coding: utf-8 -*-

import json
import os
import numpy as np
import time, datetime
from keras.preprocessing.sequence import pad_sequences
from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization, LSTM, Bidirectional, GRU
from keras.layers.embeddings import Embedding
from keras.utils.np_utils import to_categorical
from keras.regularizers import l2
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping, TensorBoard
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.engine.topology import Layer, InputSpec
from keras import initializers
from sklearn.model_selection import train_test_split
from collections import Counter
from sklearn.metrics import accuracy_score
from scipy.stats import spearmanr
from sklearn.metrics import classification_report

# Initialize global variables

DATASETS_DIR = '../../input'
TEST_FILE = 'test_20170726.json'
END_OF_STRING = ' '
TOKEN_DELIMETR = ' '
ALICE_ID = 'Alice'
BOB_ID = 'Bob'
MAX_SEQUENCE_LENGTH = 1000
VALIDATION_SPLIT = 0.1
TEST_SPLIT = 0.1
RNG_SEED = 13371447
NB_EPOCHS = 500
DROPOUT = 0.2
BATCH_SIZE = 32
OPTIMIZER = 'adadelta'
TENSORBOARD_DIR = '/tmp/keras'


SKIP_FIT = os.getenv('SKIP_FIT', False)
USE_LSTM = os.getenv('USE_LSTM', False)
if not USE_LSTM:
    MODEL_WEIGHTS_FILE = os.path.join('model_weights.h5')
else:
    MODEL_WEIGHTS_FILE = os.path.join('model_lstm_weights.h5')

#debug
import pprint
pp = pprint.PrettyPrinter(indent=4)

context = []
thread = []
thread_alice = []
thread_bob = []
dialogId = []
target_alice = []
target_bob = []
quality_alice = []
quality_bob = []

char_voc = set()

import glob
path = os.path.join(DATASETS_DIR, 'train*.json')
files=glob.glob(path)

for file_ in files:
    with open(file_, encoding='utf-8') as fh:
        data = json.load(fh)
        for row in data:
            context.append(row['context'])
            char_voc = char_voc.union(set(list(row['context'])))
            dialogId.append(row['dialogId'])

            for user in row["users"]:
                if user["id"] == ALICE_ID:
                    target_alice.append(user["userType"])
                if user["id"] == BOB_ID:
                    target_bob.append(user["userType"])
            for user in row["evaluation"]:
                if user["userId"] == ALICE_ID:
                    quality_alice.append(user["quality"])
                if user["userId"] == BOB_ID:
                    quality_bob.append(user["quality"])
            utts = []
            utts_alice = []
            utts_bob = []
            for utt in row["thread"]:
                tokenized_text = utt["text"]
                utts.append(tokenized_text)
                if utt["userId"] == ALICE_ID:
                    utts_alice.append(tokenized_text)
                if utt["userId"] == BOB_ID:
                    utts_bob.append(tokenized_text)

            char_voc = char_voc.union(set(list(END_OF_STRING.join(utts))))
            thread.append(END_OF_STRING.join(utts))
            thread_alice.append(END_OF_STRING.join(utts_alice))
            thread_bob.append(END_OF_STRING.join(utts_bob))

#load test data
def load_test(filename):
    context = []
    thread = []
    thread_alice = []
    thread_bob = []
    dialogId = []
    with open(filename, encoding='utf-8') as fh:
        data = json.load(fh)
        for row in data:
            context.append(row['context'])
            dialogId.append(row['dialogId'])

            utts = []
            utts_alice = []
            utts_bob = []
            for utt in row["thread"]:
                tokenized_text = utt["text"]
                utts.append(tokenized_text)
                if utt["userId"] == ALICE_ID:
                    utts_alice.append(tokenized_text)
                if utt["userId"] == BOB_ID:
                    utts_bob.append(tokenized_text)

            thread.append(END_OF_STRING.join(utts))
            thread_alice.append(END_OF_STRING.join(utts_alice))
            thread_bob.append(END_OF_STRING.join(utts_bob))

    return context, thread, thread_alice, thread_bob, dialogId

v = "".join(list(char_voc))
print(v.encode('ascii', 'ignore'))
chars = sorted(list(char_voc))
vocab_size = len(chars)+1
chars.insert(0, "\0")
print('total chars:', vocab_size)
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

def len_stats(arr):
        all_lens = [len(i) for i in arr]
        return np.mean(all_lens), np.max(all_lens)

def create_idx(text):
    return [char_indices[c] for c in text if c in char_indices]

def arr2idx(text_arr):
    return [create_idx(t) for t in text_arr]

thread_alice_idx = arr2idx(thread_alice)
thread_bob_idx = arr2idx(thread_bob)

print(len_stats(thread_alice_idx))
print(len_stats(thread_bob_idx))


thread_alice_idx_ = pad_sequences(thread_alice_idx, maxlen=MAX_SEQUENCE_LENGTH)
thread_bob_idx_ = pad_sequences(thread_bob_idx, maxlen=MAX_SEQUENCE_LENGTH)



def get_class_weights(y, smooth_factor=0):
    """
    Returns the weights for each class based on the frequencies of the samples
    :param smooth_factor: factor that smooths extremely uneven weights
    :param y: list of true labels (the labels must be hashable)
    :return: dictionary with the weight for each class
    """
    counter = Counter(y)

    if smooth_factor > 0:
        p = max(counter.values()) * smooth_factor
        for k in counter.keys():
            counter[k] += p

    majority = max(counter.values())

    return {cls: float(majority / count) for cls, count in counter.items()}

#labels = np.array(quality_alice, dtype=int)

#class_weights = get_class_weights(labels)
#print("Class weights {}".format(class_weights))


# Partition the dataset into train and test sets
X = np.stack((thread_alice_idx_, thread_bob_idx_), axis=1)
y = np.concatenate((to_categorical(np.array(quality_alice, dtype=int)),to_categorical(np.array(quality_bob, dtype=int))), axis=1)
print(y.shape)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SPLIT, random_state=RNG_SEED)
thread_alice_idx__train = X_train[:,0]
thread_bob_idx__train = X_train[:,1]
thread_alice_idx__test = X_test[:,0]
thread_bob_idx__test = X_test[:,1]

# Define the model
thread_alice_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
thread_bob_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
EMBEDDING_DIM = 30

GRU_UNITS = 50
LSTM_UNITS = 80


# Attention GRU network
class AttLayer(Layer):
    def __init__(self, **kwargs):
        self.init = initializers.get('normal')
        #self.input_spec = [InputSpec(ndim=3)]
        super(AttLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape)==3
        #self.W = self.init((input_shape[-1],1))
        self.W = self.init((input_shape[-1],))
        #self.input_spec = [InputSpec(shape=input_shape)]
        self.trainable_weights = [self.W]
        super(AttLayer, self).build(input_shape)  # be sure you call this somewhere!

    def call(self, x, mask=None):
        eij = K.tanh(K.dot(x, self.W))

        ai = K.exp(eij)
        weights = ai/K.sum(ai, axis=1).dimshuffle(0,'x')

        weighted_input = x*weights.dimshuffle(0,1,'x')
        return weighted_input.sum(axis=1)

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], input_shape[-1])


th_alice = Embedding(vocab_size,
                 EMBEDDING_DIM,
                 input_length=MAX_SEQUENCE_LENGTH,
                 trainable=True)(thread_alice_idx__inp)
if not USE_LSTM:
    th_alice = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th_alice)
    th_alice = Lambda(lambda x: K.max(x, axis=1), output_shape=(EMBEDDING_DIM, ))(th_alice)
else:
    th_alice = Bidirectional(LSTM(LSTM_UNITS))(th_alice)

#th_alice = Bidirectional(GRU(GRU_UNITS, return_sequences=True))(th_alice)
#th_alice = AttLayer()(th_alice)

th_bob = Embedding(vocab_size,
                 EMBEDDING_DIM,
                 input_length=MAX_SEQUENCE_LENGTH,
                 trainable=True)(thread_bob_idx__inp)

if not USE_LSTM:
    th_bob = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th_bob)
    th_bob = Lambda(lambda x: K.max(x, axis=1), output_shape=(EMBEDDING_DIM, ))(th_bob)
else:
    th_bob = Bidirectional(LSTM(LSTM_UNITS))(th_bob)

#th_bob  = Bidirectional(GRU(GRU_UNITS, return_sequences=True))(th_bob)
#th_bob = AttLayer()(th_bob)

merged = concatenate([th_alice,th_bob])
merged = Dense(200, activation='relu')(merged)
merged = Dropout(DROPOUT)(merged)
merged = BatchNormalization()(merged)
merged = Dense(200, activation='relu')(merged)
merged = Dropout(DROPOUT)(merged)
merged = BatchNormalization()(merged)
merged = Dense(200, activation='relu')(merged)
merged = Dropout(DROPOUT)(merged)
merged = BatchNormalization()(merged)
merged = Dense(200, activation='relu')(merged)
merged = Dropout(DROPOUT)(merged)
merged = BatchNormalization()(merged)

human_generated_out  = Dense(output_dim=12, activation='sigmoid')(merged)

model = Model(inputs=[thread_alice_idx__inp, thread_bob_idx__inp],  outputs=human_generated_out)
model.compile(loss='binary_crossentropy', optimizer=OPTIMIZER, metrics=['accuracy'])

# Train the model, checkpointing weights with best validation accuracy
print("Starting training at", datetime.datetime.now())
t0 = time.time()

if not USE_LSTM:
    STAMP = "concat"
else:
    STAMP = "lstm"

early_stopping = EarlyStopping(monitor='val_loss', patience=30)
model_checkpoint = ModelCheckpoint(MODEL_WEIGHTS_FILE, monitor='val_acc', save_best_only=True)
tensor_board = TensorBoard(log_dir=os.path.join(TENSORBOARD_DIR, 'human', STAMP),
                           histogram_freq=0, write_graph=True, write_images=True)

callbacks = [model_checkpoint, early_stopping, tensor_board]

if not SKIP_FIT:
    history = model.fit([thread_alice_idx__train, thread_bob_idx__train],
                        y_train,
                        epochs=NB_EPOCHS,
                        validation_split=VALIDATION_SPLIT,
                        #verbose=3,
                        #class_weight=class_weights,
                        batch_size=BATCH_SIZE,
                        callbacks=callbacks)
    t1 = time.time()
    print("Training ended at", datetime.datetime.now())
    print("Minutes elapsed: %f" % ((t1 - t0) / 60.))

    # Print best validation accuracy and epoch
    max_val_acc, idx = max((val, idx) for (idx, val) in enumerate(history.history['val_acc']))
    print('Maximum validation accuracy = {0:.4f} (epoch {1:d})'.format(max_val_acc, idx+1))
    bst_val_score = min(history.history['val_loss'])

# Evaluate the model with best validation accuracy on the test partition
model.load_weights(MODEL_WEIGHTS_FILE)
loss, accuracy = model.evaluate([thread_alice_idx__test, thread_bob_idx__test], y_test, verbose=0)
print('Test loss = {0:.4f}, test accuracy = {1:.4f}'.format(loss, accuracy))



from scipy.stats import spearmanr
import sys
import json
import csv

def evaluate_sp(train_json, out_cvs):
    users_bot_flags = {}
    with open(train_json, encoding='utf-8') as fh:
        data = json.load(fh)
        for d in data:
            if d['evaluation'][0]['userId'] == 'Alice':
                users_bot_flags[d['dialogId']] = ( (int(d['evaluation'][0]['quality']), int(d['evaluation'][1]['quality'])) )
            else:
                users_bot_flags[d['dialogId']] = ( (int(d['evaluation'][1]['quality']), int(d['evaluation'][0]['quality'])) )

    users_bot_predicted_probs = []
    users_bot_fact_labaels = []
    with open(out_cvs, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            dialog = int(row[0])
            if dialog in users_bot_flags:
                users_bot_fact_labaels.append(users_bot_flags[dialog][0])
                users_bot_predicted_probs.append(float(row[1]))

                users_bot_fact_labaels.append(users_bot_flags[dialog][1])
                users_bot_predicted_probs.append(float(row[2]))
            else:
                print("dialog %s not in dataset" % dialog, file=sys.stderr)

    print(out_cvs, ": ", spearmanr(users_bot_fact_labaels, users_bot_predicted_probs).correlation)



print("Make predictions...")

def predict(model, input, y_true):
    y_pred = model.predict(input, batch_size=BATCH_SIZE)
    #np.save('test1.npy',y_pred)
    y_pred_alice = np.argmax(y_pred[:,:6], axis=1)
    y_pred_bob = np.argmax(y_pred[:,6:], axis=1)
    y_alice = np.argmax(y_true[:,:6], axis=1)
    y_bob = np.argmax(y_true[:,6:], axis=1)
    _y_pred = np.concatenate([y_pred_alice, y_pred_bob], axis=0)
    _y_true = np.concatenate([y_alice, y_bob], axis=0)
    print("Accuracy {}, Spearman cor {}".format(
        accuracy_score(_y_true, _y_pred),
        spearmanr(_y_true, _y_pred)
        ))
    print(classification_report(_y_true, _y_pred))

predict(model, [thread_alice_idx__test, thread_bob_idx__test], y_test)
print('Predict on all dataset')
predict(model, [X[:,0], X[:,1]], y)

print("Loading test file")
test_context, test_thread, test_thread_alice, test_thread_bob, test_dialogId = load_test(os.path.join(DATASETS_DIR, TEST_FILE))
print("Got {} items in test".format(len(test_dialogId)))

def tread2idx(thread):
    thread_idx = arr2idx(thread)
    print(len_stats(thread_idx))
    return pad_sequences(thread_idx, maxlen=MAX_SEQUENCE_LENGTH)


# def fix_botbot(class_alice, class_bob, y_pred):
#     for c, y in zip(np.c_[class_alice, class_bob], y_pred):


test_thread_alice_idx_ = tread2idx(test_thread_alice)
test_thread_bob_idx_ = tread2idx(test_thread_bob)
test_y_pred = model.predict([test_thread_alice_idx_, test_thread_bob_idx_]  , batch_size=BATCH_SIZE)
test_y_pred_alice = np.argmax(test_y_pred[:,:6], axis=1)
test_y_pred_bob = np.argmax(test_y_pred[:,6:], axis=1)
test_dialogId = np.array(test_dialogId, dtype=int)

#fix_botbot(test_y_pred_alice,test_y_pred_bob,  test_y_pred )
#exit(0)

test_out = np.stack((test_dialogId, test_y_pred_alice, test_y_pred_bob), axis=1)

def save_test_csv(arr, cols=["dialogId","Alice","Bob"]):
    with open('/tmp/newbies+out26.csv', 'wb') as fp:
        #fp.write((','.join(cols) + '\n').encode())
        np.savetxt(fp, arr, fmt="%i", delimiter=",")

#save_test_csv(test_out)
evaluate_sp(os.path.join(DATASETS_DIR, 'train_20170726.json'), os.path.join(DATASETS_DIR, 'newbies+out26.csv'))
exit(0)
