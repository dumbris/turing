# -*- coding: utf-8 -*-

import json
import os
import numpy as np
import time, datetime
from keras.preprocessing.sequence import pad_sequences
from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization, LSTM, Bidirectional, GRU
from keras.layers.embeddings import Embedding
from keras.utils.np_utils import to_categorical
from keras.regularizers import l2
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping, TensorBoard
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.engine.topology import Layer, InputSpec
from keras import initializers
from sklearn.model_selection import train_test_split
from collections import Counter
from sklearn.metrics import accuracy_score
from scipy.stats import spearmanr
from sklearn.metrics import classification_report

# Initialize global variables

DATASETS_DIR = '../../input'
TEST_FILE = 'train_20170725.json'
END_OF_STRING = ' '
TOKEN_DELIMETR = ' '
ALICE_ID = 'Alice'
BOB_ID = 'Bob'
MAX_SEQUENCE_LENGTH = 1000
VALIDATION_SPLIT = 0.1
TEST_SPLIT = 0.1
RNG_SEED = 13371447
NB_EPOCHS = 500
DROPOUT = 0.2
BATCH_SIZE = 32
OPTIMIZER = 'adadelta'
TENSORBOARD_DIR = '/tmp/keras'


SKIP_FIT = os.getenv('SKIP_FIT', False)
USE_LSTM = os.getenv('USE_LSTM', False)
if not USE_LSTM:
    MODEL_WEIGHTS_FILE = os.path.join('model_weights.h5')
else:
    MODEL_WEIGHTS_FILE = os.path.join('model_lstm_weights.h5')

#debug
import pprint
pp = pprint.PrettyPrinter(indent=4)

context = []
thread = []
thread_alice = []
thread_bob = []
dialogId = []
target_alice = []
target_bob = []
quality_alice = []
quality_bob = []

char_voc = set()


v = "".join(list(char_voc))
print(v.encode('ascii', 'ignore'))
chars = sorted(list(char_voc))
vocab_size = len(chars)+1
chars.insert(0, "\0")
print('total chars:', vocab_size)
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

def len_stats(arr):
        all_lens = [len(i) for i in arr]
        return np.mean(all_lens), np.max(all_lens)

def create_idx(text):
    return [char_indices[c] for c in text]

def arr2idx(text_arr):
    return [create_idx(t) for t in text_arr]

thread_alice_idx = arr2idx(thread_alice)
thread_bob_idx = arr2idx(thread_bob)

print(len_stats(thread_alice_idx))
print(len_stats(thread_bob_idx))


thread_alice_idx_ = pad_sequences(thread_alice_idx, maxlen=MAX_SEQUENCE_LENGTH)
thread_bob_idx_ = pad_sequences(thread_bob_idx, maxlen=MAX_SEQUENCE_LENGTH)



def get_class_weights(y, smooth_factor=0):
    """
    Returns the weights for each class based on the frequencies of the samples
    :param smooth_factor: factor that smooths extremely uneven weights
    :param y: list of true labels (the labels must be hashable)
    :return: dictionary with the weight for each class
    """
    counter = Counter(y)

    if smooth_factor > 0:
        p = max(counter.values()) * smooth_factor
        for k in counter.keys():
            counter[k] += p

    majority = max(counter.values())

    return {cls: float(majority / count) for cls, count in counter.items()}

#labels = np.array(quality_alice, dtype=int)

#class_weights = get_class_weights(labels)
#print("Class weights {}".format(class_weights))


# Partition the dataset into train and test sets
X = np.stack((thread_alice_idx_, thread_bob_idx_), axis=1)
y = np.concatenate((to_categorical(np.array(quality_alice, dtype=int)),to_categorical(np.array(quality_bob, dtype=int))), axis=1)
print(y.shape)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=TEST_SPLIT, random_state=RNG_SEED)
thread_alice_idx__train = X_train[:,0]
thread_bob_idx__train = X_train[:,1]
thread_alice_idx__test = X_test[:,0]
thread_bob_idx__test = X_test[:,1]


# Train the model, checkpointing weights with best validation accuracy
print("Starting training at", datetime.datetime.now())
t0 = time.time()

if not USE_LSTM:
    STAMP = "concat"
else:
    STAMP = "lstm"

early_stopping = EarlyStopping(monitor='val_loss', patience=30)
model_checkpoint = ModelCheckpoint(MODEL_WEIGHTS_FILE, monitor='val_acc', save_best_only=True)
tensor_board = TensorBoard(log_dir=os.path.join(TENSORBOARD_DIR, 'human', STAMP),
                           histogram_freq=0, write_graph=True, write_images=True)

callbacks = [model_checkpoint, early_stopping, tensor_board]

if not SKIP_FIT:
    history = model.fit([thread_alice_idx__train, thread_bob_idx__train],
                        y_train,
                        epochs=NB_EPOCHS,
                        validation_split=VALIDATION_SPLIT,
                        #verbose=3,
                        #class_weight=class_weights,
                        batch_size=BATCH_SIZE,
                        callbacks=callbacks)
    t1 = time.time()
    print("Training ended at", datetime.datetime.now())
    print("Minutes elapsed: %f" % ((t1 - t0) / 60.))

    # Print best validation accuracy and epoch
    max_val_acc, idx = max((val, idx) for (idx, val) in enumerate(history.history['val_acc']))
    print('Maximum validation accuracy = {0:.4f} (epoch {1:d})'.format(max_val_acc, idx+1))
    bst_val_score = min(history.history['val_loss'])

# Evaluate the model with best validation accuracy on the test partition
model.load_weights(MODEL_WEIGHTS_FILE)
loss, accuracy = model.evaluate([thread_alice_idx__test, thread_bob_idx__test], y_test, verbose=0)
print('Test loss = {0:.4f}, test accuracy = {1:.4f}'.format(loss, accuracy))


print("Make predictions...")

def predict(model, input, y_true):
    y_pred = model.predict(input, batch_size=BATCH_SIZE)
    #np.save('test1.npy',y_pred)
    y_pred_alice = np.argmax(y_pred[:,:6], axis=1)
    y_pred_bob = np.argmax(y_pred[:,6:], axis=1)
    y_alice = np.argmax(y_true[:,:6], axis=1)
    y_bob = np.argmax(y_true[:,6:], axis=1)
    _y_pred = np.concatenate([y_pred_alice, y_pred_bob], axis=0)
    _y_true = np.concatenate([y_alice, y_bob], axis=0)
    print("Accuracy {}, Spearman cor {}".format(
        accuracy_score(_y_true, _y_pred),
        spearmanr(_y_true, _y_pred)
        ))
    print(classification_report(_y_true, _y_pred))

predict(model, [thread_alice_idx__test, thread_bob_idx__test], y_test)
print('Predict on all dataset')
predict(model, [X[:,0], X[:,1]], y)

exit(0)
