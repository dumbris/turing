from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization, dot, add, Flatten, Reshape, Bidirectional, LSTM
from keras.layers.embeddings import Embedding
from keras import backend as K

EMBEDDING_DIM = 30
DROPOUT = 0.2
LSTM_UNITS = 20
d_units = 15
d_units_double = d_units*2 

def input_wrapper(inp):
    return [inp.thread_alice_idx, inp.thread_bob_idx]

def model(vocab_size, MAX_SEQUENCE_LENGTH, word_embedding_matrix, nb_words):

    def EncodeInput(_inp):
        th = Embedding(vocab_size,
                     EMBEDDING_DIM,
                     input_length=MAX_SEQUENCE_LENGTH,
                     trainable=True)(_inp)
        #th = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th)
        #th = Lambda(lambda x: K.max(x, axis=1), output_shape=(EMBEDDING_DIM, ))(th)
        th = Bidirectional(LSTM(LSTM_UNITS, return_sequences=True),merge_mode="sum")(th)
        return th


    def add_attention(s1,s2):
        attention = dot([s1,s2], [1,1])
        attention = Flatten()(attention)
        attention = Dense((MAX_SEQUENCE_LENGTH*LSTM_UNITS))(attention)
        attention = Reshape((MAX_SEQUENCE_LENGTH, LSTM_UNITS))(attention)
        merged = add([s1,attention])
        merged = Flatten()(merged)
        return merged

    # Define the model
    thread_alice_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
    thread_bob_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))

    th_alice = EncodeInput(thread_alice_idx__inp)
    th_bob = EncodeInput(thread_bob_idx__inp)

    merged = add_attention(th_alice,th_bob)
  
    merged = Dense(d_units_double, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(d_units_double, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(d_units_double, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(d_units_double, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)

    human_generated_out  = Dense(output_dim=12, activation='sigmoid')(merged)

    model = Model(inputs=[thread_alice_idx__inp, thread_bob_idx__inp],  outputs=human_generated_out)
    return model
