from keras.models import Model
from keras.layers import Input, Dense, concatenate, \
    Dropout, BatchNormalization, LSTM, Bidirectional
from keras.layers.embeddings import Embedding

EMBEDDING_DIM = 50
DROPOUT = 0.2
LSTM_UNITS = 64

def input_wrapper(inp):
    return [inp.thread_alice_idx, inp.thread_bob_idx]

def model(vocab_size, MAX_SEQUENCE_LENGTH, word_embedding_matrix, nb_words):

    def EncodeInput(_inp):
        th = Embedding(vocab_size,
                     EMBEDDING_DIM,
                     input_length=MAX_SEQUENCE_LENGTH,
                     trainable=True)(_inp)

        th = Bidirectional(LSTM(LSTM_UNITS))(th)
        return th

    # Define the model
    thread_alice_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
    thread_bob_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))

    th_alice = EncodeInput(thread_alice_idx__inp)
    th_bob = EncodeInput(thread_bob_idx__inp)

    merged = concatenate([th_alice,th_bob])
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)

    human_generated_out  = Dense(output_dim=12, activation='sigmoid')(merged)

    model = Model(inputs=[thread_alice_idx__inp, thread_bob_idx__inp],  outputs=human_generated_out)
    return model
