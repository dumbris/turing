from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization
from keras.layers.embeddings import Embedding
from keras import backend as K

EMBEDDING_DIM = 50
DROPOUT = 0.2

def input_wrapper(inp):
    return [inp.context_wordidx, inp.thread_alice_idx, inp.thread_bob_idx]

def model(vocab_size, MAX_SEQUENCE_LENGTH, word_embedding_matrix, nb_words):

    def EncodeInput(_inp):
        th = Embedding(vocab_size,
                     EMBEDDING_DIM,
                     input_length=MAX_SEQUENCE_LENGTH,
                     trainable=True)(_inp)
        th = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th)
        th = Lambda(lambda x: K.max(x, axis=1), output_shape=(EMBEDDING_DIM, ))(th)
        return th

    def EncodeInputWord(_inp):

        th = Embedding(nb_words + 1,
                         EMBEDDING_DIM,
                         weights=[word_embedding_matrix],
                         input_length=MAX_SEQUENCE_LENGTH,
                         trainable=False)(_inp)
        th = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th)
        th = Lambda(lambda x: K.max(x, axis=1), output_shape=(EMBEDDING_DIM, ))(th)
        return th

    # Define the model
    context_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
    thread_alice_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
    thread_bob_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))

    context = EncodeInputWord(context_idx__inp)
    th_alice = EncodeInput(thread_alice_idx__inp)
    th_bob = EncodeInput(thread_bob_idx__inp)

    merged = concatenate([context,th_alice,th_bob])
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(200, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)

    human_generated_out  = Dense(output_dim=12, activation='sigmoid')(merged)

    model = Model(inputs=[context_idx__inp, thread_alice_idx__inp, thread_bob_idx__inp],  outputs=human_generated_out)
    return model
