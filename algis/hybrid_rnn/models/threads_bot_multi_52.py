from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization
from keras.layers.embeddings import Embedding
from keras import backend as K

EMBEDDING_DIM = 30
DROPOUT = 0.2
DENSE_SIZE = 100

def input_wrapper(inp):
    return [inp.thread_alice_idx, inp.thread_bob_idx]

def model(vocab_size, MAX_SEQUENCE_LENGTH, word_embedding_matrix, nb_words):

    def EncodeInput(_inp):
        th = Embedding(vocab_size,
                     EMBEDDING_DIM,
                     input_length=MAX_SEQUENCE_LENGTH,
                     trainable=True)(_inp)
        th = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th)
        th = Lambda(lambda x: K.max(x, axis=1), output_shape=(EMBEDDING_DIM, ))(th)
        return th

    # Define the model
    thread_alice_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))
    thread_bob_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,))

    th_alice = EncodeInput(thread_alice_idx__inp)
    th_bob = EncodeInput(thread_bob_idx__inp)

    merged = concatenate([th_alice,th_bob])
    merged = Dense(DENSE_SIZE, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(DENSE_SIZE, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    alice_bot_out  = Dense(output_dim=2, activation='softmax')(merged)
    bob_bot_out  = Dense(output_dim=2, activation='softmax')(merged)
    merged = Dense(DENSE_SIZE, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)
    merged = Dense(DENSE_SIZE, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)

    alice_quality_out  = Dense(output_dim=6, activation='softmax')(merged)
    bob_quality_out  = Dense(output_dim=6, activation='softmax')(merged)

    model = Model(inputs=[thread_alice_idx__inp, thread_bob_idx__inp],  outputs=[alice_quality_out,
                                                                                 bob_quality_out,
                                                                                 alice_bot_out,
                                                                                 bob_bot_out
                                                                                 ])
    return model
