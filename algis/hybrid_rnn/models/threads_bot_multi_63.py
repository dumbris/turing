from keras.models import Model
from keras.layers import Input, TimeDistributed, Dense, Lambda, concatenate, \
    Dropout, BatchNormalization
from keras.layers.embeddings import Embedding
from keras import backend as K

EMBEDDING_DIM = 35
DROPOUT = 0.07
DENSE_SIZE = 350

def input_wrapper(inp):
    return [inp.thread_alice_idx, inp.thread_bob_idx]

def model(vocab_size, MAX_SEQUENCE_LENGTH, word_embedding_matrix, nb_words):

    def EncodeInput(_inp, name):
        th = Embedding(vocab_size,
                     EMBEDDING_DIM,
                     input_length=MAX_SEQUENCE_LENGTH,
                     trainable=True)(_inp)
        th = TimeDistributed(Dense(EMBEDDING_DIM, activation='relu'))(th)
        th = Lambda(lambda x: K.mean(x, axis=1), output_shape=(EMBEDDING_DIM, ), name="Mean_"+name)(th)
        return th



    # Define the model
    thread_alice_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,), name="thread_Alice")
    thread_bob_idx__inp = Input(shape=(MAX_SEQUENCE_LENGTH,), name="thread_Bob")

    th_alice = EncodeInput(thread_alice_idx__inp, "Alice")
    th_bob = EncodeInput(thread_bob_idx__inp, "Bob")



    merged = concatenate([th_alice,th_bob])
    merged = Dense(DENSE_SIZE, activation='relu')(merged)
    merged = Dropout(DROPOUT)(merged)
    merged = BatchNormalization()(merged)

    alice_quality_out  = Dense(output_dim=6, activation='softmax', name="quality_Alice")(merged)
    bob_quality_out  = Dense(output_dim=6, activation='softmax', name="quality_Bob")(merged)
    alice_bot_out  = Dense(output_dim=2, activation='softmax', name="Alice_bot")(merged)
    bob_bot_out  = Dense(output_dim=2, activation='softmax', name="Bob_bot")(merged)

    model = Model(inputs=[thread_alice_idx__inp, thread_bob_idx__inp],  outputs=[alice_quality_out,
                                                                                 bob_quality_out,
                                                                                 alice_bot_out,
                                                                                 bob_bot_out
                                                                                 ])
    return model
