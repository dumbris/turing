import numpy as np

def fix_bot(out):
    test_dialogId_, y_pred_alice_, y_pred_bob_, y_pred_alice_bot_, y_pred_bob_bot_ = [], [], [], [], []
    for test_dialogId, y_pred_alice, y_pred_bob, y_pred_alice_bot, y_pred_bob_bot in out:
        test_dialogId_.append(test_dialogId)
        if y_pred_alice_bot == 1 and y_pred_alice > 0:
            print("Alice {} {} {} {} {}".format(test_dialogId, y_pred_alice, y_pred_bob, y_pred_alice_bot, y_pred_bob_bot))
            y_pred_alice_.append(0)
        else:
            y_pred_alice_.append(y_pred_alice)
        if y_pred_bob_bot == 1 and y_pred_bob > 0:
            print("Bob {} {} {} {} {}".format(test_dialogId, y_pred_alice, y_pred_bob, y_pred_alice_bot, y_pred_bob_bot))
            y_pred_bob_.append(0)
        else:
            y_pred_bob_.append(y_pred_bob)
        y_pred_alice_bot_.append(y_pred_alice_bot)
        y_pred_bob_bot_.append(y_pred_bob_bot)
    return np.stack((np.array(test_dialogId_),
                np.array(y_pred_alice_),
                np.array(y_pred_bob_),
                np.array(y_pred_alice_bot_),
                np.array(y_pred_bob_bot_)
                ), axis=1)

