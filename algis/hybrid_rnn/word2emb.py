from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import numpy as np
import os

MAX_NB_WORDS = 15000
GLOVE_DIR = '../../input'
GLOVE_FILE = 'glove.6B.50d.txt'
EMBEDDING_DIM = 50
MAX_SEQUENCE_LENGTH = 1000

def arr2wordidx(ds, tokenizer = None):
    # Build tokenized word index
    if not tokenizer:
        tokenizer = Tokenizer(num_words=MAX_NB_WORDS)
        texts = ds.context + ds.thread
        tokenizer.fit_on_texts(texts)
    context_word_sequences = tokenizer.texts_to_sequences(ds.context)
    alice_word_sequences = tokenizer.texts_to_sequences(ds.thread_alice)
    bob_word_sequences = tokenizer.texts_to_sequences(ds.thread_bob)
    word_index = tokenizer.word_index

    def len_stats(arr):
        all_lens = [len(i) for i in arr]
        return np.mean(all_lens), np.max(all_lens)

    print("Context sequence mean %d, max %d" % len_stats(context_word_sequences))
    print("Alice sequence mean %d, max %d" % len_stats(alice_word_sequences))
    print("Bob sequence mean %d, max %d" % len_stats(bob_word_sequences))

    print("Words in index: %d" % len(word_index))

    print("Processing", GLOVE_FILE)

    embeddings_index = {}
    with open(os.path.join(GLOVE_DIR, GLOVE_FILE), encoding='utf-8') as f:
        for line in f:
            values = line.split(' ')
            word = values[0]
            embedding = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = embedding

    print('Word embeddings: %d' % len(embeddings_index))

    # Prepare word embedding matrix
    nb_words = min(MAX_NB_WORDS, len(word_index))
    word_embedding_matrix = np.zeros((nb_words + 1, EMBEDDING_DIM))
    for word, i in word_index.items():
        if i > MAX_NB_WORDS:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            word_embedding_matrix[i] = embedding_vector

    print('Null word embeddings: %d' % np.sum(np.sum(word_embedding_matrix, axis=1) == 0))

    # Prepare training data tensors
    context_wordidx = pad_sequences(context_word_sequences, maxlen=MAX_SEQUENCE_LENGTH)
    thread_alice_wordidx = pad_sequences(alice_word_sequences, maxlen=MAX_SEQUENCE_LENGTH)
    thread_bob_wordidx = pad_sequences(bob_word_sequences, maxlen=MAX_SEQUENCE_LENGTH)
    print('Shape of context data tensor:', context_wordidx.shape)
    print('Shape of alice thread data tensor:', thread_alice_wordidx.shape)
    print('Shape of bob thread data tensor:', thread_bob_wordidx.shape)
    return context_wordidx, thread_alice_wordidx, thread_bob_wordidx, word_embedding_matrix, nb_words, tokenizer


