import numpy as np
from collections import namedtuple
from sklearn.model_selection import train_test_split
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from word2emb import arr2wordidx

TEST_SPLIT = 0.1
RNG_SEED = 13371447
SKIP_WORD_LEVEL = False

Inp = namedtuple('inp', [
                            'context_idx',
                            'thread_idx',
                            'thread_alice_idx',
                            'thread_bob_idx',
                            'context_wordidx',
                            'thread_alice_wordidx',
                            'thread_bob_wordidx',
                            ])
Target = namedtuple('target', ['quality_alice', 'quality_bob', 'alice_bot', 'bob_bot'])

def concat_X_inp(inp):
    return \
    np.stack((
            inp.context_idx,
            inp.thread_alice_idx,
            inp.thread_bob_idx,
            inp.context_wordidx,
            inp.thread_alice_wordidx,
            inp.thread_bob_wordidx,
            ), axis=1)

def concat_y(target):
    #return np.concatenate((target.quality_alice, target.quality_bob, target.alice_bot, target.bob_bot), axis=1)
    #return np.stack((target.quality_alice, target.quality_bob, target.alice_bot, target.bob_bot), axis=1)
    return [target.quality_alice, target.quality_bob, target.alice_bot, target.bob_bot]

def split_test_train(X,y):
    # Partition the dataset into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(concat_X_inp(X), concat_y(y), test_size=TEST_SPLIT, random_state=RNG_SEED)
    train = Inp(  X_train[:,0],
                X_train[:,1],
                X_train[:,2],
                X_train[:,3],
                X_train[:,4],
                X_train[:,5]
                )
    test = Inp(  X_test[:,0],
                X_test[:,1],
                X_test[:,2],
                X_test[:,3],
                X_test[:,4],
                X_test[:,5]
                )
    return train, test, y_train, y_test


def ds2inp(ds, ct, MAX_SEQUENCE_LENGTH, _tokenizer = None):
    context_idx = ct.arr2idx(ds.context)
    thread_idx = ct.arr2idx(ds.thread)
    thread_alice_idx = ct.arr2idx(ds.thread_alice)
    thread_bob_idx = ct.arr2idx(ds.thread_bob)

    print(ct.len_stats(context_idx))
    print(ct.len_stats(thread_idx))
    print(ct.len_stats(thread_alice_idx))
    print(ct.len_stats(thread_bob_idx))

    context_idx_ = pad_sequences(context_idx, maxlen=MAX_SEQUENCE_LENGTH)
    thread_idx_ = pad_sequences(thread_idx, maxlen=MAX_SEQUENCE_LENGTH)
    thread_alice_idx_ = pad_sequences(thread_alice_idx, maxlen=MAX_SEQUENCE_LENGTH)
    thread_bob_idx_ = pad_sequences(thread_bob_idx, maxlen=MAX_SEQUENCE_LENGTH)

    #encode words
    #TODO
    context_wordidx, \
    thread_alice_wordidx, \
    thread_bob_wordidx, \
    word_embedding_matrix, nb_words, tokenizer = arr2wordidx(ds, _tokenizer)


    inp = Inp(  context_idx_,
                thread_idx_,
                thread_alice_idx_,
                thread_bob_idx_,
                context_wordidx,
                thread_alice_wordidx,
                thread_bob_wordidx
                )
    if ds.format == 'train':
        y = Target(
                to_categorical(np.array(ds.quality_alice, dtype=int)),
                to_categorical(np.array(ds.quality_bob, dtype=int)),
                to_categorical(np.array(ds.target_alice, dtype=int)),
                to_categorical(np.array(ds.target_bob, dtype=int))
                )
    else:
        y = None
    return inp, y, word_embedding_matrix, nb_words, tokenizer
