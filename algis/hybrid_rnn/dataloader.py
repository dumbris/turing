from pathlib import Path
import glob

import json
import os
import numpy as np
import time, datetime
from collections import namedtuple

TRAIN_FORMAT = "train"
TEST_FORMAT = "test"
END_OF_STRING = ' '
TOKEN_DELIMETR = ' '
ALICE_ID = 'Alice'
BOB_ID = 'Bob'
USERTYPE_BOT = 'Bot'
USERTYPE_HUMAN = 'Human'

Dataset = namedtuple('Dataset', [
                                    'format',
                                    'dialogId',
                                    'context',
                                    'thread',
                                    'thread_alice',
                                    'thread_bob',
                                    'quality_alice',
                                    'quality_bob',
                                    'target_alice',
                                    'target_bob',
                                    ])

def usertype2bool(userType):
    if userType == USERTYPE_BOT:
        return 1
    return 0


def load_json(file_, format=TRAIN_FORMAT):
    dialogId = []
    context = []
    thread = []
    thread_alice = []
    thread_bob = []
    target_alice = []
    target_bob = []
    quality_alice = []
    quality_bob = []
    with open(file_, encoding='utf-8') as fh:
        data = json.load(fh)
        for row in data:
            context.append(row['context'])
            #char_voc = char_voc.union(set(list(row['context'])))
            dialogId.append(row['dialogId'])

            if format==TRAIN_FORMAT:
                for user in row["users"]:
                    if user["id"] == ALICE_ID:
                        target_alice.append(usertype2bool(user["userType"]))
                    if user["id"] == BOB_ID:
                        target_bob.append(usertype2bool(user["userType"]))
                for user in row["evaluation"]:
                    if user["userId"] == ALICE_ID:
                        quality_alice.append(user["quality"])
                    if user["userId"] == BOB_ID:
                        quality_bob.append(user["quality"])
            utts = []
            utts_alice = []
            utts_bob = []
            for utt in row["thread"]:
                tokenized_text = utt["text"]
                utts.append(tokenized_text)
                if utt["userId"] == ALICE_ID:
                    utts_alice.append(tokenized_text)
                if utt["userId"] == BOB_ID:
                    utts_bob.append(tokenized_text)

            #char_voc = char_voc.union(set(list(END_OF_STRING.join(utts))))
            thread.append(END_OF_STRING.join(utts))
            thread_alice.append(END_OF_STRING.join(utts_alice))
            thread_bob.append(END_OF_STRING.join(utts_bob))

        return Dataset(format,
                    dialogId,
                    context,
                    thread,
                    thread_alice,
                    thread_bob,
                    quality_alice,
                    quality_bob,
                    target_alice,
                    target_bob,
                    )


def get_train(path: Path, mask="train*.json", skip=[]):
    files = glob.glob(str(path / Path(mask)))
    dialogId = []
    context = []
    thread = []
    thread_alice = []
    thread_bob = []
    target_alice = []
    target_bob = []
    quality_alice = []
    quality_bob = []
    for file_ in files:
        if Path(file_).name in skip:
            print("file {} skiped".format(file_))
            continue
        ds = load_json(file_)
        dialogId        += ds.dialogId
        context         += ds.context
        thread          += ds.thread
        thread_alice    += ds.thread_alice
        thread_bob      += ds.thread_bob
        target_alice    += ds.target_alice
        target_bob      += ds.target_bob
        quality_alice   += ds.quality_alice
        quality_bob     += ds.quality_bob
    print("Loaded {} items from {}".format(len(dialogId), str(path / mask)))

    return Dataset(TRAIN_FORMAT,
            dialogId,
            context,
            thread,
            thread_alice,
            thread_bob,
            quality_alice,
            quality_bob,
            target_alice,
            target_bob)

def get_test_file(filepath: Path):
    ds = load_json(str(filepath), TEST_FORMAT)
    print("Loaded {} items from {}".format(len(ds.dialogId), str(filepath)))
    return ds

def get_train_file(filepath: Path):
    ds = load_json(str(filepath), TRAIN_FORMAT)
    print("Loaded {} items from {}".format(len(ds.dialogId), str(filepath)))
    return ds
