from scipy.stats import spearmanr
import json, sys, csv

def spirmaneval(train_json, out_cvs):
    users_bot_flags = {}
    with open(train_json, encoding='utf-8') as fh:
        data = json.load(fh)
        for d in data:
            if d['evaluation'][0]['userId'] == 'Alice':
                users_bot_flags[d['dialogId']] = ( (int(d['evaluation'][0]['quality']), int(d['evaluation'][1]['quality'])) )
            else:
                users_bot_flags[d['dialogId']] = ( (int(d['evaluation'][1]['quality']), int(d['evaluation'][0]['quality'])) )

    users_bot_predicted_probs = []
    users_bot_fact_labaels = []
    with open(out_cvs, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            dialog = int(row[0])
            if dialog in users_bot_flags:
                users_bot_fact_labaels.append(users_bot_flags[dialog][0])
                users_bot_predicted_probs.append(float(row[1]))

                users_bot_fact_labaels.append(users_bot_flags[dialog][1])
                users_bot_predicted_probs.append(float(row[2]))
            else:
                print("dialog %s not in dataset" % dialog, file=sys.stderr)

    print(out_cvs, ": ", spearmanr(users_bot_fact_labaels, users_bot_predicted_probs).correlation)
