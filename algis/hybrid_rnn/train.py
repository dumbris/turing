import argparse
from pathlib import Path
import os
from dataloader import get_train, get_test_file#, get_train_file
import numpy as np
from chartable import CharTable
import time, datetime
from keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard#, LearningRateScheduler
from input import ds2inp, concat_y
from spirmaneval import spirmaneval
import importlib
from filters import fix_bot
from keras.utils import plot_model


DATASETS_DIR = Path('../../input')
#TEST_FILE = Path('test_20170727.json')
TEST_FILE = Path('test_final.json')
TRAIN_FILE = Path('train_final.json')
OUT_FILE = Path('newbies_final.csv')
MAX_SEQUENCE_LENGTH = 600
VALIDATION_SPLIT = 0.1
RNG_SEED = 13371447
NB_EPOCHS = 200
BATCH_SIZE = 16
OPTIMIZER = 'adadelta'
#OPTIMIZER = 'adam'
TENSORBOARD_DIR = '/tmp/keras'
#TODO: it should depend on model name
BASE_MODEL_WEIGHTS_FILE = "model"

def save_test_csv(arr, filepath, header=False, cols=["dialogId","Alice","Bob"]):
    with open(str(filepath), 'wb') as fp:
        if header:
            fp.write((','.join(cols) + '\n').encode())
        np.savetxt(fp, arr, fmt="%i", delimiter=",")


def get_model_filename(base, module, model, count, ext = "h5"):
    return "{}_{}_{}_{}.{}".format(base, module, model, count, ext)

def pred2vals(y_pred):
    quality_alice = y_pred[:,[0,1,2,3,4,5]]
    quality_bob = y_pred[:,[6,7,8,9,10,11]]
    alice_bot = y_pred[:,[12,13]]
    bob_bot = y_pred[:,[14,15]]
    return quality_alice, quality_bob, alice_bot, bob_bot

# # learning rate schedule
# def step_decay(epoch):
# 	initial_lrate = 0.5
# 	drop = 0.1
# 	epochs_drop = 5.0
# 	lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
# 	return lrate

def main():
    parser = argparse.ArgumentParser()
    arg = parser.add_argument
    arg('--mode', choices=['train', 'valid', 'predict_valid', 'predict_test', 'test', 'submit'],
        default='train')
    arg('--module', type=str, help='use module', default='models.threads_rnn')
    arg('--model', type=str, help='use model', default='model')
    arg('--skip', type=str, help='skip json')
    args = parser.parse_args()


    #load train data
    ds = get_train(DATASETS_DIR, skip=[args.skip])
    train_items_count = len(ds.dialogId)
    #encode chars into indicies
    ct = CharTable()
    ct.add_chars_from_arr(ds.context)
    ct.add_chars_from_arr(ds.thread)
    #print(ct)

    #Transform dataset into input format
    inp, y, word_embedding_matrix, nb_words, tokenizer = ds2inp(ds, ct, MAX_SEQUENCE_LENGTH)

    #inp_train, inp_test, y_train, y_test = split_test_train(inp, y)
    inp_train = inp
    y_train = concat_y(y)

    #load model
    if args.model:
        module = importlib.import_module(args.module)
        model = getattr(module, args.model)(ct.vocab_size, MAX_SEQUENCE_LENGTH, word_embedding_matrix, nb_words)
        input_wrapper = getattr(module, 'input_wrapper')
        MODEL_WEIGHTS_FILE = get_model_filename(BASE_MODEL_WEIGHTS_FILE,
                                                args.module,
                                                args.model,
                                                train_items_count)
    else:
        exit(1)
        #model = threads_rnn(ct.vocab_size, MAX_SEQUENCE_LENGTH)

    model.compile(loss='binary_crossentropy', optimizer=OPTIMIZER, metrics=['accuracy'])
    #print(model.summary())
    plot_model(model, show_layer_names=True, show_shapes=False, to_file=get_model_filename('diagram', args.module, args.model, '_', 'png'))

    if args.mode == 'train':

        # Train the model, checkpointing weights with best validation accuracy
        print("Starting training at", datetime.datetime.now())
        t0 = time.time()
        early_stopping = EarlyStopping(monitor='loss', patience=5)
        model_checkpoint = ModelCheckpoint(MODEL_WEIGHTS_FILE, monitor='loss', save_best_only=True)
        tensor_board = TensorBoard(log_dir=os.path.join(TENSORBOARD_DIR, 'turing', args.module),
                                   histogram_freq=0, write_graph=True, write_images=True)

        # learning schedule callback
        #lrate = LearningRateScheduler(step_decay)

        callbacks = [model_checkpoint, early_stopping, tensor_board]



        _ = model.fit(input_wrapper(inp_train),
                            y_train,
                            epochs=NB_EPOCHS,
                            validation_split=VALIDATION_SPLIT,
                            verbose=3,
                            batch_size=BATCH_SIZE,
                            callbacks=callbacks)
        t1 = time.time()
        print("Training ended at", datetime.datetime.now())
        print("Minutes elapsed: %f" % ((t1 - t0) / 60.))

        # Print best validation accuracy and epoch
        #max_val_acc, idx = max((val, idx) for (idx, val) in enumerate(history.history['val_acc']))
        #print('Maximum validation accuracy = {0:.4f} (epoch {1:d})'.format(max_val_acc, idx+1))
        #bst_val_score = min(history.history['val_loss'])

    #elif args.mode == 'test':
        model.load_weights(MODEL_WEIGHTS_FILE)
        test_ds = get_test_file(DATASETS_DIR / TEST_FILE)
        #test_ds = get_train_file(DATASETS_DIR / TRAIN_FILE)
        test_inp, test_y, _, _, _ = ds2inp(test_ds, ct, MAX_SEQUENCE_LENGTH, tokenizer)
        quality_alice, quality_bob, alice_bot, bob_bot = model.predict(input_wrapper(test_inp), batch_size=BATCH_SIZE)
        #quality_alice, quality_bob, alice_bot, bob_bot = pred2vals(y_pred)
        y_pred_alice = np.argmax(quality_alice, axis=1)
        y_pred_bob = np.argmax(quality_bob, axis=1)
        #y_pred_alice_bot = np.argmax(alice_bot, axis=1)
        #y_pred_bob_bot = np.argmax(bob_bot, axis=1)
        test_dialogId = np.array(test_ds.dialogId, dtype=int)
        #test_out = np.stack((test_dialogId, y_pred_alice, y_pred_bob, y_pred_alice_bot, y_pred_bob_bot  ), axis=1)
        test_out = np.stack((test_dialogId, y_pred_alice, y_pred_bob), axis=1)
        #test_out = fix_bot(test_out)
        save_test_csv(test_out, str(DATASETS_DIR / OUT_FILE))
        spirmaneval(str(DATASETS_DIR / TRAIN_FILE), str(str(DATASETS_DIR / OUT_FILE)))

    elif args.mode == 'submit':
        model.load_weights(MODEL_WEIGHTS_FILE)
        test_ds = get_test_file(DATASETS_DIR / TEST_FILE)
        test_inp, test_y, _, _, _ = ds2inp(test_ds, ct, MAX_SEQUENCE_LENGTH, tokenizer)
        y_pred = model.predict(input_wrapper(test_inp), batch_size=BATCH_SIZE)
        quality_alice, quality_bob, alice_bot, bob_bot = pred2vals(y_pred)
        y_pred_alice = np.argmax(quality_alice, axis=1)
        y_pred_bob = np.argmax(quality_bob, axis=1)
        #y_pred_alice_bot = np.argmax(alice_bot, axis=1)
        #y_pred_bob_bot = np.argmax(bob_bot, axis=1)
        test_dialogId = np.array(test_ds.dialogId, dtype=int)
        test_out = np.stack((test_dialogId, y_pred_alice, y_pred_bob), axis=1)
        fix_bot(test_out)
        save_test_csv(test_out, str(DATASETS_DIR / OUT_FILE), header=True)


# def fix_botbot(dialogId, y_pred):
# for row in np.stack((dialogId, y_pred), axis=1):
#     _id = row[:,0]
#     y = row[:,1]

#exit(0)





if __name__ == '__main__':
    main()
