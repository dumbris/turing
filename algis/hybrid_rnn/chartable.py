
import numpy as np


class CharTable:

    def __init__(self):
        self.char_voc = set()
        self.char_indices = dict()
        self.indices_char = dict()
        self.vocab_size = 0

    def add_chars(self, arr):
        self.char_voc = self.char_voc.union(set(list(arr)))
        self.update_indices()

    def add_chars_from_arr(self, arr2d):
        self.add_chars([item for sublist in arr2d for item in sublist])

    def update_indices(self):
        chars = sorted(list(self.char_voc))
        self.vocab_size = len(chars)+1
        chars.insert(0, "\0")
        print('total chars:', self.vocab_size)
        self.char_indices = dict((c, i) for i, c in enumerate(chars))
        self.indices_char = dict((i, c) for i, c in enumerate(chars))

    def create_idx(self, text):
        cs = [c for c in text if c not in self.char_indices]
        if len(cs) > 0:
            try:
                print("Unknown chars {}".format(self.chars_encode(cs)))
            except Exception as e:
                print(e)
        return [self.char_indices[c] for c in text if c in self.char_indices]

    def arr2idx(self, text_arr):
        return [self.create_idx(t) for t in text_arr]

    def chars_encode(self, arr):
        v = "".join(list(arr))
        return v.encode('utf-8')

    def __str__(self):
        return "{}".format(self.chars_encode(self.char_voc))


    def len_stats(self, arr):
            all_lens = [len(i) for i in arr]
            return np.mean(all_lens), np.max(all_lens)


